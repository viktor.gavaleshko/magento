<?php
declare(strict_types=1);

namespace MyProject\StorelocatorElogic\Console;

use Exception;
use Magento\Framework\File\Csv;
use MyProject\StorelocatorElogic\Helper\CreateUniqueUrl;
use MyProject\StorelocatorElogic\Model\ResourceModel\Storelocator\CollectionFactory;
use MyProject\StorelocatorElogic\Model\StorelocatorFactory;
use MyProject\StorelocatorElogic\Model\StorelocatorRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCSV extends Command
{
    /**
     * @var Csv
     */
    protected $csv;

    /**
     * @var StorelocatorFactory
     */
    protected $storelocatorFactory;

    /**
     * @var StorelocatorRepository
     */
    protected $storelocatorRepository;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var CreateUniqueUrl
     */
    private $createuniqueurl;

    /**
     * ImportCSV constructor.
     * @param Csv $csv
     * @param StorelocatorFactory $storelocatorFactory
     * @param StorelocatorRepository $storelocatorRepository
     * @param CollectionFactory $collectionFactory
     * @param CreateUniqueUrl $createuniqueurl
     */
    public function __construct(
        Csv $csv,
        StorelocatorFactory $storelocatorFactory,
        StorelocatorRepository $storelocatorRepository,
        CollectionFactory $collectionFactory,
        CreateUniqueUrl $createuniqueurl
    ) {
        $this->csv = $csv;
        $this->storelocatorFactory = $storelocatorFactory;
        $this->storelocatorRepository = $storelocatorRepository;
        $this->collectionFactory = $collectionFactory;
        $this->createuniqueurl = $createuniqueurl;

        parent::__construct();
    }

    /**
     * @return void
     */
    protected function configure()
    {
        $this->setName('storelocatorelogic:import:csv');
        $this->setDescription('Import CSV file');
        $this->addOption('path', "p", InputOption::VALUE_OPTIONAL, "Path for csv file");
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $path = $input->getOption('path');

        if (empty($path)) {
            throw  new Exception("Invalid argument");
        }

        try {
            $csvData = $this->csv->getData($path);
            $keys = array_shift($csvData);
            $keys = array_flip($keys);

            foreach ($csvData as $value) {
                if ($value[$keys['url_key']] == 0) {
                    $urlkey = $this->createuniqueurl->createUrl($value[$keys['name']]);
                }

                $store = $this->storelocatorFactory->create()
                    ->setNameStore($value[$keys['name']])
                    ->setAddress($value[$keys['address']])
                    ->setDescription($value[$keys['description']])
                    ->setImages($value[$keys['store_img']])
                    ->setWorkSchedule($value[$keys['schedule']])
                    ->setUrlKey($urlkey);
                $this->storelocatorRepository->save($store);
            }
            $output->writeln('Import Done!');
        } catch (Exception $e) {
            $output->writeln('Invalid CSV');
            $output->writeln($e->getMessage());
        }
    }
}
