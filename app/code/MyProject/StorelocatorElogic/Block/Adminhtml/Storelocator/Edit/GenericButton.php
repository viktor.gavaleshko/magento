<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MyProject\StorelocatorElogic\Block\Adminhtml\Storelocator\Edit;

use Magento\Backend\Block\Widget\Context;
use MyProject\StorelocatorElogic\Api\StorelocatorRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 * @package MyProject\StorelocatorElogic\Block\Adminhtml\Storelocator\Edit
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var StorelocatorRepositoryInterface
     */
    protected $storelocatorRepository;

    /**
     * GenericButton constructor.
     *
     * @param Context $context
     * @param StorelocatorRepositoryInterface $storelocatorRepository
     */
    public function __construct(
        Context $context,
        StorelocatorRepositoryInterface $storelocatorRepository
    ) {
        $this->context = $context;
        $this->storelocatorRepository = $storelocatorRepository;
    }

    /**
     * Return storelocator store ID
     *
     * @return string|null
     */
    public function getStorelocatorId(): ?string
    {
        try {
            return $this->storelocatorRepository->getById(
                $this->context->getRequest()->getParam('storelocator_id')
            )->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = []): string
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
