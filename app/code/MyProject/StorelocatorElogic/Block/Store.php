<?php

declare(strict_types=1);

namespace MyProject\StorelocatorElogic\Block;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use MyProject\StorelocatorElogic\Api\Data\StorelocatorInterface;
use MyProject\StorelocatorElogic\Model\Storelocator;
use MyProject\StorelocatorElogic\Model\StorelocatorRepository;

/**
 * Class Stores
 * @package MyProject\StorelocatorElogic\Block
 */
class Store extends Template
{
    /**
     * @var RequestInterface
     */
    protected $request;
    /**
     * @var StorelocatorRepository
     */
    private $storelocatorRepository;

    /**
     * Storelocator constructor.
     * @param StorelocatorRepository $storelocatorRepository
     * @param Template\Context $context
     * @param RequestInterface $request
     * @param array $data
     */
    public function __construct(
        StorelocatorRepository $storelocatorRepository,
        Template\Context $context,
        RequestInterface $request,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->request = $request;
        $this->storelocatorRepository = $storelocatorRepository;
    }

    /**
     * @return StorelocatorInterface
     * @throws NoSuchEntityException
     */
    public function getStore(): StorelocatorInterface
    {
        $storelocator_id = $this->request->getParam('id');
        return $this->storelocatorRepository->getById($storelocator_id);
    }

}
