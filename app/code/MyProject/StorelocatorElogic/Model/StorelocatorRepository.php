<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MyProject\StorelocatorElogic\Model;

use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Reflection\DataObjectProcessor;
use MyProject\StorelocatorElogic\Api\Data;
use MyProject\StorelocatorElogic\Api\Data\StorelocatorInterface;
use MyProject\StorelocatorElogic\Api\StorelocatorRepositoryInterface;
use MyProject\StorelocatorElogic\Model\ResourceModel\Storelocator as ResourceStorelocator;
use MyProject\StorelocatorElogic\Model\ResourceModel\Storelocator\CollectionFactory as StorelocatorCollectionFactory;

/**
 * Class StorelocatorRepository
 * @package MyProject\StorelocatorElogic\Model
 */
class StorelocatorRepository implements StorelocatorRepositoryInterface
{
    /**
     * @var array
     */
    private $registry = [];

    /**
     * @var ResourceStorelocator
     */
    protected $resource;

    /**
     * @var StorelocatorFactory
     */
    protected $storelocatorFactory;

    /**
     * @var StorelocatorCollectionFactory
     */
    protected $storelocatorCollectionFactory;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * StorelocatorRepository constructor.
     * @param ResourceStorelocator $resource
     * @param StorelocatorFactory $storelocatorFactory
     * @param StorelocatorCollectionFactory $storelocatorCollectionFactory
     */
    public function __construct(
        ResourceStorelocator $resource,
        StorelocatorFactory $storelocatorFactory,
        StorelocatorCollectionFactory $storelocatorCollectionFactory
    ) {
        $this->resource = $resource;
        $this->storelocatorFactory = $storelocatorFactory;
        $this->storelocatorCollectionFactory = $storelocatorCollectionFactory;
    }

    /**
     * Save store data
     *
     * @param StorelocatorInterface|Storelocator $storelocator
     * @return StorelocatorInterface
     * @throws StateException
     */
    public function save(Data\StorelocatorInterface $storelocator): StorelocatorInterface
    {
        /* try {
             $this->resource->save($storelocator);
         } catch (\Exception $exception) {
             throw new CouldNotSaveException(__($exception->getMessage()));
         }
         return $storelocator;*/

        try {
            /** @var Storelocator $storelocator */
            $this->resource->save($storelocator);
            $this->registry[$storelocator->getId()] = $this->getById($storelocator->getId());
        } catch (\Exception $exception) {
            throw new StateException(__('Unable to save post #%1', $storelocator->getId()));
        }
        return $this->registry[$storelocator->getId()];
    }

    /**
     * Load store data by given store Identity
     *
     * @param int $storelocatorId
     * @return StorelocatorInterface
     * @throws NoSuchEntityException
     */
    public function getById($storelocatorId): StorelocatorInterface
    {
        $storelocator = $this->storelocatorFactory->create();
        $this->resource->load($storelocator, $storelocatorId);
        if (!$storelocator->getId()) {
            throw new NoSuchEntityException(__('The storelocator with the "%1" ID doesn\'t exist.', $storelocatorId));
        }
        return $storelocator;
    }

    /**
     * @return \MyProject\StorelocatorElogic\Api\Data\StorelocatorInterface[]
     */
    public function getList()
    {
        $stores = [];
        $storelocator = $this->storelocatorCollectionFactory->create();
        foreach ($storelocator as $item) {
            $stores[] = $item;
        }
        return $stores;
    }

    /**
     * Delete store
     *
     * @param StorelocatorInterface|Storelocator $storelocator
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(Data\StorelocatorInterface $storelocator): bool
    {
        try {
            $this->resource->delete($storelocator);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * Delete store by given store Identity
     *
     * @param int $storelocatorId
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($storelocatorId): bool
    {
        return $this->delete($this->getById($storelocatorId));
    }
}
