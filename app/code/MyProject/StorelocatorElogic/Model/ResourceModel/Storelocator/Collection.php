<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MyProject\StorelocatorElogic\Model\ResourceModel\Storelocator;

use Exception;
use Magento\Cms\Model\ResourceModel\AbstractCollection;
use Magento\Store\Model\Store;
use MyProject\StorelocatorElogic\Api\Data\StorelocatorInterface;

/**
 * Class Collection
 * @package MyProject\StorelocatorElogic\Model\ResourceModel\Storelocator
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'storelocator_id';

    /**
     * Load data for preview flag
     *
     * @var bool
     */
    protected $_previewFlag;

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'storelocatorelogic_storelocator_collection';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'storelocator_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\MyProject\StorelocatorElogic\Model\Storelocator::class, \MyProject\StorelocatorElogic\Model\ResourceModel\Storelocator::class);
    }

    /**
     * Set first store flag
     *
     * @param bool $flag
     * @return $this
     */
    public function setFirstStoreFlag($flag = false): Collection
    {
        $this->_previewFlag = $flag;
        return $this;
    }

    /**
     * Add filter by store
     *
     * @param int|array|Store $store
     * @param bool $withAdmin
     * @return $this
     */
    public function addStoreFilter($store, $withAdmin = true): Collection
    {
        if (!$this->getFlag('store_filter_added')) {
            $this->performAddStoreFilter($store, $withAdmin);
            $this->setFlag('store_filter_added', true);
        }
        return $this;
    }

    /**
     * Perform operations after collection load
     *
     * @return Collection
     * @throws Exception
     */
    protected function _afterLoad(): Collection
    {
        $entityMetadata = $this->metadataPool->getMetadata(StorelocatorInterface::class);
        $this->performAfterLoad('myproject_storelocatorelogic_system', $entityMetadata->getLinkField());
        $this->_previewFlag = false;

        return parent::_afterLoad();
    }

    /**
     * Perform operations before rendering filters
     *
     * @throws Exception
     */
    protected function _renderFiltersBefore(): void
    {
        $entityMetadata = $this->metadataPool->getMetadata(StorelocatorInterface::class);
        $this->joinStoreRelationTable('myproject_storelocatorelogic_system', $entityMetadata->getLinkField());
    }
}
