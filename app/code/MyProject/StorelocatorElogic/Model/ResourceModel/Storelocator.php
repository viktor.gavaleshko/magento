<?php
declare(strict_types=1);

namespace MyProject\StorelocatorElogic\Model\ResourceModel;

/**
 * Class Storelocator
 * @package MyProject\StorelocatorElogic\Model\ResourceModel
 */
class Storelocator extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('myproject_storelocatorelogic_system', 'storelocator_id');
    }
}
