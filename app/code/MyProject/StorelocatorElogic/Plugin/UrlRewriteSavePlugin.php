<?php

declare(strict_types=1);

namespace MyProject\StorelocatorElogic\Plugin;

use Exception;
use Magento\Store\Model\ResourceModel\Store\Collection as StoreCollection;
use Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollectionFactory;
use MyProject\StorelocatorElogic\Api\Data\StorelocatorInterface;
use MyProject\StorelocatorElogic\Api\StorelocatorRepositoryInterface;
use MyProject\StorelocatorElogic\Model\StorelocatorRepository;

/**
 * Class UrlRewriteSavePlugin
 * @package MyProject\StorelocatorElogic\Plugin
 */
class UrlRewriteSavePlugin
{
    protected const REQUEST_PATH = 'store/';

    protected const TARGET_PATH = 'storelocator/store/index/id/';

    /**
     * @var \Magento\UrlRewrite\Model\UrlRewriteFactory
     */
    protected $_urlRewriteFactory;

    /**
     * @var UrlRewriteCollectionFactory
     */
    protected $urlRewriteCollectionFactory;

    /**
     * @var StorelocatorRepositoryInterface
     */
    protected $storelocatorRepository;

    /**
     * @var
     */
    protected $storeCollection;

    /**
     * UrlRewriteSavePlugin constructor.
     * @param \Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewriteFactory
     * @param UrlRewriteCollectionFactory $urlRewriteCollectionFactory
     * @param StorelocatorRepositoryInterface $storelocatorRepository
     * @param StoreCollection $storeCollection
     */
    public function __construct(
        \Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewriteFactory,
        UrlRewriteCollectionFactory $urlRewriteCollectionFactory,
        StorelocatorRepositoryInterface $storelocatorRepository,
        StoreCollection $storeCollection
    ) {
        $this->urlRewriteCollectionFactory = $urlRewriteCollectionFactory;
        $this->_urlRewriteFactory = $urlRewriteFactory;
        $this->storelocatorRepository = $storelocatorRepository;
        $this->storeCollection = $storeCollection;
    }

    /**
     * @param StorelocatorRepository $subject
     * @param StorelocatorInterface $storelocator
     * @return StorelocatorInterface
     * @throws Exception
     */
    public function afterSave(StorelocatorRepository $subject, StorelocatorInterface $storelocator): StorelocatorInterface
    {
        $id = $storelocator->getId();
        $urlKey = $storelocator->getUrlKey();
        $targetPath = (self::TARGET_PATH . $id . "/");
        $requestPath = (self::REQUEST_PATH . $urlKey . "/");
        $collection = $this->urlRewriteCollectionFactory->create();
        $collection->addFieldToFilter("target_path", ["eq"=>$targetPath]);
        //$collection->addFieldToFilter("request_path", ["neq" => $requestPath]);
        $collection->getSelect();
        if ($collection->count()) {
            foreach ($collection as $item) {
                if ($item->getRequestPath() != $requestPath) {
                    $requestPathNew = (self::REQUEST_PATH . $urlKey . "/");
                    $item->setRequestPath($requestPathNew);
                    $item->save();
                }
            }
        } elseif (!$collection->count() && $id != 0) {
            $stores = $this->storeCollection->load();
            foreach ($stores as $store) {
                $urlRewriteModel = $this->_urlRewriteFactory->create();
                $urlRewriteModel->setStoreId($store->getStoreId());
                $urlRewriteModel->setIsSystem(0);
                $urlRewriteModel->setIdPath(rand(1, 100000));
                $urlRewriteModel->setTargetPath($targetPath);
                $urlRewriteModel->setRequestPath($requestPath);
                $urlRewriteModel->save();
            }
        }
        return $storelocator;
    }
}
