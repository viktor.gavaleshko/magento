<?php

declare(strict_types=1);

namespace MyProject\StorelocatorElogic\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\TemporaryState\CouldNotSaveException;
use MyProject\StorelocatorElogic\Helper\CreateUniqueUrl;
use MyProject\StorelocatorElogic\Helper\GoogleApi;
use MyProject\StorelocatorElogic\Model\ResourceModel\Storelocator\CollectionFactory;

/**
 * Class AutoChangeCoordinateAndUrlKey
 * @package MyProject\StorelocatorElogic\Observer
 */
class AutoChangeCoordinateAndUrlKey implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var GoogleApi
     */
    private $googleApi;

    /**
     * @var CreateUniqueUrl
     */
    private $createuniqueurl;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    private $messageManager;

    public function __construct(
        CreateUniqueUrl $createuniqueurl,
        GoogleApi $googleApi,
        CollectionFactory $collectionFactory
    ) {
        $this->createuniqueurl = $createuniqueurl;
        $this->googleApi = $googleApi;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @param Observer $observer
     * @return $this|void
     * @throws LocalizedException
     */
    public function execute(Observer $observer): AutoChangeCoordinateAndUrlKey
    {
        $storelocator = $observer->getEvent()->getObject();
        if ($storelocator->getLatitude() == 0 || $storelocator->getLongitude() == 0) {
            try {
                $googleapi = $this->googleApi->getGoogleApi($storelocator->getAddress());
                $storelocator->setLongitude($googleapi['longitude'])->setLatitude($googleapi['latitude']);
            } catch (CouldNotSaveException $exception) {
                $this->messageManager->addNoticeMessage($exception->getMessage());
            }
        }

        if (strlen($storelocator->getUrlKey()) == 0) {
            try {
                $urlkey = $this->createuniqueurl->createUrl($storelocator->getNameStore());
                $storelocator->setUrlKey($urlkey);
            } catch (CouldNotSaveException $exception) {
                $this->messageManager->addNoticeMessage($exception->getMessage());
            }
        }
        return $this;
    }
}
