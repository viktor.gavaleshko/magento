<?php
declare(strict_types=1);

namespace MyProject\StorelocatorElogic\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class Data
 * @package MyProject\StorelocatorElogic\Helper
 */
class Data
{
    const STORELOCATORELOGIC_STORELOCATOR_GENERAL_API_KEY = "storelocator/general/api_key";

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * Data constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->scopeConfig->getValue(self::STORELOCATORELOGIC_STORELOCATOR_GENERAL_API_KEY);
    }

}
