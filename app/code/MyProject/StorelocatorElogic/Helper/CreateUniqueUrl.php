<?php
declare(strict_types=1);

namespace MyProject\StorelocatorElogic\Helper;

use MyProject\StorelocatorElogic\Model\ResourceModel\Storelocator\CollectionFactory;

class CreateUniqueUrl
{
    protected const TABLE_NAME = 'myproject_storelocatorelogic_system';

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var GetAutoIncremet
     */
    private $getAutoIncremet;

    /**
     * CreateUniqueUrl constructor.
     * @param CollectionFactory $collectionFactory
     * @param GetAutoIncremet $getAutoIncremet
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        GetAutoIncremet $getAutoIncremet
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->getAutoIncremet = $getAutoIncremet;
    }

    public function createUrl($namestorenew): string
    {
        $dictionaryDell = ["!", ",", ".", "@", "#", "$", "%", "^", "'", ")", "&"];
        $urlkey = $namestorenew;
        $urlkey = str_replace($dictionaryDell, " ", $urlkey);
        $urlkey = str_replace(" ", "_", $urlkey);
        $urlkey = strtolower($urlkey);

        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('name_store', ['eq' => $namestorenew]);

        if ($collection->count()) {
            $urlkey = $urlkey . "_" . $collection->count();
            $count = 0;
        }

        $collection2 = $this->collectionFactory->create();
        $collection2->addFieldToFilter("url_key", ["eq" => $urlkey]);
        $collection2->getSelect();

        if ($collection2->count() > 0) {
            $id = $this->getAutoIncremet->getNextAutoincrement(self::TABLE_NAME);
            $urlkey = $urlkey . "_" . $id;
        }

        return $urlkey;
    }
}
