<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MyProject\StorelocatorElogic\Api;

/**
 * Interface StorelocatorRepositoryInterface
 * @package MyProject\StorelocatorElogic\Api
 */
interface StorelocatorRepositoryInterface
{
    /**
     * Save store
     *
     * @param Data\StorelocatorInterface $storelocator
     * @return \MyProject\StorelocatorElogic\Api\Data\StorelocatorInterface
     */
    public function save(Data\StorelocatorInterface $storelocator): \MyProject\StorelocatorElogic\Api\Data\StorelocatorInterface;

    /**
     * Retrieve store
     *
     * @param int $storelocatorId
     * @return \MyProject\StorelocatorElogic\Api\Data\StorelocatorInterface
     */
    public function getById($storelocatorId): \MyProject\StorelocatorElogic\Api\Data\StorelocatorInterface;

    /**
     * Retrieve stores
     *
     * @return \MyProject\StorelocatorElogic\Api\Data\StorelocatorInterface[]
     */
    public function getList();

    /**
     * Delete store
     *
     * @param Data\StorelocatorInterface $storelocator
     * @return bool
     */
    public function delete(Data\StorelocatorInterface $storelocator): bool;

    /**
     * Delete store by ID.
     *
     * @param int $storelocatorId
     * @return bool
     */
    public function deleteById($storelocatorId): bool;
}
