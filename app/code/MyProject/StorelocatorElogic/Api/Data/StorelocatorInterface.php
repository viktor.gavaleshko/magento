<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MyProject\StorelocatorElogic\Api\Data;

/**
 * StorelocatorElogic store Interface
 * @package MyProject\StorelocatorElogic\Api\Data
 */
interface StorelocatorInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const STORELOCATOR_ID = 'storelocator_id';
    const NAME_STORE      = 'name_store';
    const DESCRIPTION     = 'description';
    const IMAGES          = 'images';
    const ADDRESS         = 'address';
    const URL_KEY         = 'url_key';
    const WORK_SCHEDULE   = 'work_schedule';
    const LONGITUDE       = 'longitude';
    const LATITUDE        = 'latitude';

    /**
     * Get ID
     *
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * Get name store
     *
     * @return string|null
     */
    public function getNameStore(): ?string;

    /**
     * Get description
     *
     * @return string|null
     */
    public function getDescription(): ?string;

    /**
     * Get images
     *
     * @return string|null
     */
    public function getImages(): ?string;

    /**
     * Get address
     *
     * @return string|null
     */
    public function getAddress(): ?string;

    /**
     * Get urlkey
     *
     * @return string|null
     */
    public function getUrlKey(): ?string;

    /**
     * Get work schedule
     *
     * @return string|null
     */
    public function getWorkSchedule(): ?string;

    /**
     * Get longitude
     *
     * @return string|null
     */
    public function getLongitude(): ?string;

    /**
     * Get latitude
     *
     * @return string|null
     */
    public function getLatitude(): ?string;

    /**
     * Set ID
     * @param int $id
     * @return StorelocatorInterface
     */
    public function setId($id): StorelocatorInterface;

    /**
     * Set store name
     *
     * @param $nameStore
     * @return StorelocatorInterface
     */
    public function setNameStore($nameStore): StorelocatorInterface;

    /**
     * Set description
     *
     * @param $description
     * @return StorelocatorInterface
     */
    public function setDescription($description): StorelocatorInterface;

    /**
     * Set images
     *
     * @param $images
     * @return StorelocatorInterface
     */
    public function setImages($images): StorelocatorInterface;

    /**
     * Set address
     *
     * @param $address
     * @return StorelocatorInterface
     */
    public function setAddress($address): StorelocatorInterface;

    /**
     * Set url key
     *
     * @param $url_key
     * @return StorelocatorInterface
     */
    public function setUrlKey($url_key): StorelocatorInterface;

    /**
     * Set work schedule
     *
     * @param $workSchedule
     * @return StorelocatorInterface
     */
    public function setWorkSchedule($workSchedule): StorelocatorInterface;

    /**
     * Set longitude
     *
     * @param $longitude
     * @return StorelocatorInterface
     */
    public function setLongitude($longitude): StorelocatorInterface;

    /**
     * Set latitude
     *
     * @param $latitude
     * @return StorelocatorInterface
     */
    public function setLatitude($latitude): StorelocatorInterface;
}
