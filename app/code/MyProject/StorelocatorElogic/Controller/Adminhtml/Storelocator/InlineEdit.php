<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MyProject\StorelocatorElogic\Controller\Adminhtml\Storelocator;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use MyProject\StorelocatorElogic\Api\Data\StorelocatorInterface;
use MyProject\StorelocatorElogic\Api\StorelocatorRepositoryInterface as StorelocatorRepository;

/**
 * Class InlineEdit
 * @package MyProject\StorelocatorElogic\Controller\Adminhtml\Storelocator
 */
class InlineEdit extends Storelocator
{
    /**
     * @var \Magento\Cms\Api\BlockRepositoryInterface
     */
    protected $storelocatorRepository;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $jsonFactory;

    /**
     * @param Context $context
     * @param StorelocatorRepository $storelocatorRepository
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,
        StorelocatorRepository $storelocatorRepository,
        JsonFactory $jsonFactory
    ) {
        parent::__construct($context);
        $this->storelocatorRepository = $storelocatorRepository;
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * @return ResultInterface
     * @throws LocalizedException
     */
    public function execute(): ResultInterface
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        if ($this->getRequest()->getParam('isAjax')) {
            $postItems = $this->getRequest()->getParam('items', []);
            if (!count($postItems)) {
                $messages[] = __('Please correct the data sent.');
                $error = true;
            } else {
                foreach (array_keys($postItems) as $storelocatorId) {
                    /** @var \Magento\Cms\Model\Block $storelocator */
                    $storelocator = $this->storelocatorRepository->getById($storelocatorId);
                    try {
                        $storelocator->setData(array_merge($storelocator->getData(), $postItems[$storelocatorId]));
                        $this->storelocatorRepository->save($storelocator);
                    } catch (\Exception $e) {
                        $messages[] = $this->getErrorWithStorelocatorId(
                            $storelocator,
                            __($e->getMessage())
                        );
                        $error = true;
                    }
                }
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    /**
     * Add storelocator title to error message
     *
     * @param StorelocatorInterface $storelocator
     * @param string $errorText
     * @return string
     */
    protected function getErrorWithStorelocatorId(StorelocatorInterface $storelocator, $errorText): string
    {
        return '[Storelocator ID: ' . $storelocator->getId() . '] ' . $errorText;
    }
}
