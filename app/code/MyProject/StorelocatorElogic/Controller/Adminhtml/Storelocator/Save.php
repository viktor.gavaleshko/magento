<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MyProject\StorelocatorElogic\Controller\Adminhtml\Storelocator;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use MyProject\StorelocatorElogic\Api\StorelocatorRepositoryInterface;
use MyProject\StorelocatorElogic\Model\ImageUploader;
use MyProject\StorelocatorElogic\Model\Storelocator;
use MyProject\StorelocatorElogic\Model\StorelocatorFactory;

/**
 * Save storelocator action.
 * Class Save
 * @package MyProject\StorelocatorElogic\Controller\Adminhtml\Storelocator
 */
class Save extends \MyProject\StorelocatorElogic\Controller\Adminhtml\Storelocator\Storelocator implements HttpPostActionInterface
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var StorelocatorFactory
     */
    private $storelocatorFactory;

    /**
     * @var StorelocatorRepositoryInterface
     */
    private $storelocatorRepository;

    /**
     * @var \Mageprince\Faq\Model\ImageUploader
     */
    protected $imageUploader;

    /**
     * Save constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param DataPersistorInterface $dataPersistor
     * @param StorelocatorFactory|null $storelocatorFactory
     * @param ImageUploader $imageUploader
     * @param StorelocatorRepositoryInterface|null $storelocatorRepository
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        DataPersistorInterface $dataPersistor,
        StorelocatorFactory $storelocatorFactory = null,
        ImageUploader $imageUploader,
        StorelocatorRepositoryInterface $storelocatorRepository = null
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->imageUploader = $imageUploader;
        $this->storelocatorFactory = $storelocatorFactory
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(StorelocatorFactory::class);
        $this->storelocatorRepository = $storelocatorRepository
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(StorelocatorRepositoryInterface::class);
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            if (empty($data['storelocator_id'])) {
                $data['storelocator_id'] = null;
            }

            /** @var Storelocator $model */
            $model = $this->storelocatorFactory->create();

            $id = $this->getRequest()->getParam('storelocator_id');
            if ($id) {
                try {
                    $model = $this->storelocatorRepository->getById($id);
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This storelocator no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }

            try {
                $data = $this->_filterFoodData($data);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*//*/');
            }

            $model->setData($data);

            try {
                $this->storelocatorRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the store.'));
                $this->dataPersistor->clear('myproject_storelocatorelogic_system');
                return $this->processStorelocatorReturn($model, $data, $resultRedirect);
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the store.'));
            }

            $this->dataPersistor->set('myproject_storelocatorelogic_system', $data);
            return $resultRedirect->setPath('*/*/edit', ['storelocator_id' => $id]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Process and set the storelocator return
     *
     * @param $model
     * @param $data
     * @param $resultRedirect
     * @return mixed
     */
    private function processStorelocatorReturn($model, $data, $resultRedirect)
    {
        $redirect = $data['back'] ?? 'close';

        if ($redirect ==='continue') {
            $resultRedirect->setPath('*/*/edit', ['storelocator_id' => $model->getId()]);
        } elseif ($redirect === 'close') {
            $resultRedirect->setPath('*/*/');
        } elseif ($redirect === 'duplicate') {
            $duplicateModel = $this->storelocatorFactory->create(['data' => $data]);
            $duplicateModel->setId(null);
            $this->storelocatorRepository->save($duplicateModel);
            $id = $duplicateModel->getId();
            $this->messageManager->addSuccessMessage(__('You duplicated the store.'));
            $this->dataPersistor->set('myproject_storelocatorelogic_system', $data);
            $resultRedirect->setPath('*/*/edit', ['storelocator_id' => $id]);
        }
        return $resultRedirect;
    }

    public function _filterFoodData(array $rawData)
    {
        $data = $rawData;

        $images_str = '';
        if (isset($data['images'])) {
            foreach ($data['images'] as $key => $image) {
                if (isset($image['name'])) {
                    $images_str = $images_str . $image['name'] . ';';
                }
                if (isset($image['images'])) {
                    $images_str = $images_str . $image['images'] . ";";
                }
            }
        }
        if (!empty($images_str)) {
            $images_str = mb_substr($images_str, 0, -1);
            $data['images'] = $images_str;
        } else {
            $data['images'] = null;
        }
        return $data;
    }

}
